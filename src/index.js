import PanoHandler from './PanoHandler';
function init() {
    const pano = settings.panoHandler = new PanoHandler({ container: document.querySelector('#sitePanos') });
    pano.init();
}
document.addEventListener('DOMContentLoaded', init);
