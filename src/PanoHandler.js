import panoData from './panoData';

class PanoHandler {
  constructor(props) {
    this.ready = false;
    this.firstTime = true;
    this.webMSupported = true;

    this.showInfo = false; //shows overlay
    this.fixDistortion = true;
    this.fov = (/(iPhone|iPod)/.test(navigator.userAgent) || /android/i.test(navigator.userAgent)) ? 52 : 99;
    // this.fov = 88;

    this.hotspotData = panoData.hotpsots;
    this.panos = panoData.panos;
    this.panorama = {};
    this.hotspots = [];
    this.hoverlabel = [];
    this.viewer = window.viewer = {};
    this.container = props.container || document.querySelector('#sitePanos');
  }
  init() {
    this.supportsWebMAlpha(
      (response => {
        // console.log('supportsWebMAlpha', response);
        this.webMSupported = response;
        this.initPano();
      }).bind(this)
    );
  }
  initPano() {
    window.addEventListener(
      'resize',
      this.onResize.bind(this)
    );
    window.addEventListener(
      'orientationchange',
      (() => {
        setTimeout(
          this.onResize, 500);
      }).bind(this)
    );
    this.panos.forEach(obj => {
      if (obj.visible) this.panorama[obj.name] = new PANOLENS.ImagePanorama(obj.source);
    });

    this.addPanoObjects(this.hotspotData);
    const viewOptions = {
      cameraFov: this.fov,
      controlBar: false,
      viewIndicator: false,
      autoHideInfospot: false,
      container: this.container,
      output: 'console'
    };

    this.viewer = new PANOLENS.Viewer(viewOptions);

    //SET VIEWER LIMITS
    this.viewer.OrbitControls.minPolarAngle = Math.PI / 2.5;
    this.viewer.OrbitControls.maxPolarAngle = Math.PI * 0.5125;

    //FOCUS THE VIEWER ON THE FIRST HOTSPOT
    this.panorama['home'].addEventListener(
      'enter',
      (() => {
        let newLoc = new THREE.Vector3(...this.hotspotData[0].position);

        this.viewer.tweenControlCenter(newLoc, 0);
      }).bind(this)
    );

    this.viewer.OrbitControls.noZoom = true;

    // CHANGE CONTROLS FOR MOBILE
    if (settings.isMobile) {
      try {
        this.viewer.enableControl(PANOLENS.Controls.DEVICEORIENTATION);
      } catch (e) {}
    }
    this.viewer.control.noZoom = true;

    // ADD PANOS TO VIEWER
    Object.keys(this.panorama).forEach(obj => {
      this.viewer.add(this.panorama[obj]);
    });

    this.panoContainer = this.viewer.getContainer();
    this.renderer = this.viewer.getRenderer();
    this.scene = this.viewer.getScene();
    this.camera = this.viewer.getCamera();

    this.fixDistortion();
    // adding environment, dust plane if needed
    // this.addEnviro();

    //EVENTLISTENS
    // this.container = this.viewer.getContainer();
    // this.panoContainer.addEventListener('mousedown', this.onUserInteraction.bind(this));
    // this.panoContainer.addEventListener('touchstart', this.onUserInteraction.bind(this));

    // IF ANY OF THE PANO OBJECTS NEED TO BE MOVED
    // this.editInfospots(true, 3);

    this.ready = true;
    this.startPanoUpdate();

    console.log(this.hotspots);
  }
  // METHODS

  fixDistortion() {
    if (this.fixDistortion) {
      this.renderPass = new THREE.RenderPass(this.scene, this.camera);
      this.composer = new THREE.EffectComposer(this.renderer);
      this.composer.addPass(this.renderPass);

      this.effect = new THREE.ShaderPass(this.getDistortionShaderDefinition());
      this.composer.addPass(this.effect);
      this.effect.renderToScreen = true;

      if (this.showInfo) {
        this.guiParameters = {
          horizontalFOV: 140,
          strength: 0.5,
          cylindricalRatio: 2
        };

        var info = document.createElement('div');
        info.id = 'info';
        document.body.appendChild(info);

        this.gui = new dat.GUI({ width: 320 });
        this.gui.add(this.guiParameters, 'horizontalFOV', 5, 160, 1).onChange(this.updateDistortionEffect.bind(this));
        this.gui.add(this.guiParameters, 'strength', 0.0, 1.0, 0.025).onChange(this.updateDistortionEffect.bind(this));
        this.gui
          .add(this.guiParameters, 'cylindricalRatio', 0.25, 4.0, 0.025)
          .onChange(this.updateDistortionEffect.bind(this));
      }

      this.updateDistortionEffect();
    }
  }

  updateDistortionEffect() {
    var height =
      Math.tan(THREE.Math.degToRad(this.showInfo ? this.guiParameters.horizontalFOV : this.fov) / 2) /
      this.camera.aspect;
    // var height = Math.tan(THREE.Math.degToRad(this.guiParameters.horizontalFOV) / 2) / this.camera.aspect;

    this.camera.fov = (Math.atan(height) * 2 * 180) / 3.1415926535;
    this.camera.updateProjectionMatrix();
    if (this.showInfo) {
      this.effect.uniforms['strength'].value = this.guiParameters.strength;
      this.effect.uniforms['height'].value = height;
      this.effect.uniforms['aspectRatio'].value = this.camera.aspect;
      this.effect.uniforms['cylindricalRatio'].value = this.guiParameters.cylindricalRatio;
    } else {
      this.effect.uniforms['strength'].value = 0.5;
      this.effect.uniforms['height'].value = height;
      this.effect.uniforms['aspectRatio'].value = this.camera.aspect;
      this.effect.uniforms['cylindricalRatio'].value = 0.5;
    }
  }

  getDistortionShaderDefinition() {
    return {
      uniforms: {
        tDiffuse: { type: 't', value: null },
        strength: { type: 'f', value: 0 },
        height: { type: 'f', value: 1 },
        aspectRatio: { type: 'f', value: 1 },
        cylindricalRatio: { type: 'f', value: 1 }
      },

      vertexShader: [
        'uniform float strength;', // s: 0 = perspective, 1 = stereographic
        'uniform float height;', // h: tan(verticalFOVInRadians / 2)
        'uniform float aspectRatio;', // a: screenWidth / screenHeight
        'uniform float cylindricalRatio;', // c: cylindrical distortion ratio. 1 = spherical

        'varying vec3 vUV;', // output to interpolate over screen
        'varying vec2 vUVDot;', // output to interpolate over screen

        'void main() {',
        'gl_Position = projectionMatrix * (modelViewMatrix * vec4(position, 1.0));',

        'float scaledHeight = strength * height;',
        'float cylAspectRatio = aspectRatio * cylindricalRatio;',
        'float aspectDiagSq = aspectRatio * aspectRatio + 1.0;',
        'float diagSq = scaledHeight * scaledHeight * aspectDiagSq;',
        'vec2 signedUV = (2.0 * uv + vec2(-1.0, -1.0));',

        'float z = 0.5 * sqrt(diagSq + 1.0) + 0.5;',
        'float ny = (z - 1.0) / (cylAspectRatio * cylAspectRatio + 1.0);',

        'vUVDot = sqrt(ny) * vec2(cylAspectRatio, 1.0) * signedUV;',
        'vUV = vec3(0.5, 0.5, 1.0) * z + vec3(-0.5, -0.5, 0.0);',
        'vUV.xy += uv;',
        '}'
      ].join('\n'),

      fragmentShader: [
        'uniform sampler2D tDiffuse;', // sampler of rendered scene�s render target
        'varying vec3 vUV;', // interpolated vertex output data
        'varying vec2 vUVDot;', // interpolated vertex output data

        'void main() {',
        'vec3 uv = dot(vUVDot, vUVDot) * vec3(-0.5, -0.5, -1.0) + vUV;',
        'gl_FragColor = texture2DProj(tDiffuse, uv);',
        '}'
      ].join('\n')
    };
  }

  addEnviro() {
    var dustTexture = new THREE.TextureLoader().load('assets/images/home/dust-blurry.png');

    // dustTexture.needsUpdate;
    dustTexture.wrapS = THREE.MirroredRepeatWrapping;
    dustTexture.wrapT = THREE.MirroredRepeatWrapping;
    dustTexture.repeat.set(6, 6);
    dustTexture.minFilter = THREE.LinearFilter;
    dustTexture.magFilter = THREE.LinearFilter;
    dustTexture.format = THREE.RGBAFormat;
    dustTexture.crossOrigin = 'anonymous';

    this.dust = {};
    this.dust.el = new THREE.Mesh(
      new THREE.PlaneGeometry(1600, 1000),
      new THREE.MeshBasicMaterial({ map: dustTexture, transparent: true })
    );
    dustTexture.repeat.set(4, 4);

    this.dust.el2 = new THREE.Mesh(
      new THREE.PlaneGeometry(1600, 1000),
      new THREE.MeshBasicMaterial({ map: dustTexture, transparent: true })
    );
    var hoverDustBox = new THREE.Box3().setFromObject(this.dust.el);
    var hoverDustBox2 = new THREE.Box3().setFromObject(this.dust.el2);
    hoverDustBox.getCenter(this.dust.el.position);
    hoverDustBox2.getCenter(this.dust.el2.position);

    this.dust.el.position.multiplyScalar(-1);
    this.dust.pivot = new THREE.Group();
    this.dust.pivot.add(this.dust.el);
    this.dust.pivot.position.set(1840, 370, -40);

    this.dust.el.material.opacity = 0.8;
    this.dust.pivot.rotation.y = -1.4;
    this.dust.pivot.scale.x = 3.3;
    this.dust.pivot.scale.y = 3.3;
    // this.dust.pivot.position.set(...this.hotspotData[0].position);
    // this.dust.pivot.rotation.x = Math.random()*30;

    this.dust.el2.position.multiplyScalar(-1);
    this.dust.pivot2 = new THREE.Group();
    this.dust.pivot2.add(this.dust.el2);
    this.dust.pivot2.position.set(-1280, 30, 0);

    this.dust.el2.material.opacity = 0.8;
    this.dust.pivot2.rotation.y = -4.79;
    this.dust.pivot2.scale.x = 2.4;
    this.dust.pivot2.scale.y = 2.4;

    // this.dust.pivot2.rotation.y = Math.random()*30;

    this.panorama['home'].add(this.dust.pivot);
    this.panorama['home'].add(this.dust.pivot2);
  }
  startPanoUpdate() {
    // if(this.viewer)
    try {
      this.viewer.addUpdateCallback(this.onPanoChange.bind(this));
      this.enableUpdate = true;
    } catch (e) {
      console.log('error caught', e);
    }
  }
  stopPanoUpdate() {
    // if(this.viewer)
    try {
      this.viewer.removeUpdateCallback(this.onPanoChange.bind(this));
      this.enableUpdate = false;
    } catch (e) {
      console.log('error caught', e);
    }
  }
  changePano(ref) {
    console.log('changePano', ref, this.hotspots[ref]);
    const newPano = this.panorama[this.hotspots[ref].target];
    if (newPano !== 'home') {
      this.viewer.enableAutoRate();
      this.viewer.animate();
    } else {
      this.viewer.disableAutoRate();
    }
    this.viewer.setPanorama(newPano);
  }

  changePanoByName(name) {
    this.viewer.setPanorama(this.panorama[name]);
  }
  panToObject(objName) {
    function findByTarget(val) {
      this.hotspotData.forEach(v => {
        if (v.target == val) {
          return v;
        }
      });
    }
    let newLoc = new THREE.Vector3(findByTarget(objName).position);
    this.viewer.tweenControlCenter(newLoc, 1500, Expo.easeOut);
  }

  showPanoObjects() {
    this.hotspots.forEach(obj => {
      const el = obj.pivot || obj.el || null;
      if (el !== null) el.visible = true;
    });
    this.startPanoUpdate();
  }

  hidePanoObjects() {
    this.stopPanoUpdate();
    this.hotspots.forEach(obj => {
      const el = obj.pivot || obj.el || null;
      if (el !== null) el.visible = false;
    });
  }
  addPanoObjects(data) {
    data.forEach((obj, idx) => {
      if (!obj.visible) return;

      this.hotspots[idx] = {};

      switch (obj.type) {
        case 'link':
          // this.panorama[obj.pano].link(obj.target, new THREE.Vector3(...obj.position),10);
          this.hotspots[idx].el = new PANOLENS.Infospot(obj.scale, Preloader.getFile(obj.art), false);
          this.hotspots[idx].el.position.set(...obj.position);
          this.hotspots[idx].el.setText(obj.label);
          this.hotspots[idx].target = obj.target;
          this.hotspots[idx].el.addEventListener(
            'click',
            (() => {
              if (!this.enableUpdate) return;
              this.changePano(idx);
            }).bind(this)
          );
          if (typeof this.panorama[obj.pano] !== 'undefined' && typeof this.panorama[obj.target] !== 'undefined')
            this.panorama[obj.pano].add(this.hotspots[idx].el);
          break;
          case 'url':
            this.hotspots[idx].el = new PANOLENS.Infospot(obj.scale, obj.art, false);
            this.hotspots[idx].el.position.set(...obj.position);
            this.hotspots[idx].target = obj.target;
            this.hotspots[idx].el.addEventListener(
              'click',
              (() => {
                if (!this.enableUpdate) return;
                window.open(this.hotspots[idx].target);
              }).bind(this)
            );
  
            if (typeof this.panorama[obj.pano] !== 'undefined') this.panorama[obj.pano].add(this.hotspots[idx].el);
  
            break;
  
  
        case 'section':
          this.hotspots[idx].el = new PANOLENS.Infospot(obj.scale, obj.art, false);
          this.hotspots[idx].el.position.set(...obj.position);
          this.hotspots[idx].target = obj.target;
          this.hotspots[idx].el.addEventListener(
            'click',
            (() => {
              if (!this.enableUpdate) return;

              console.log(`hotspot ref #: ${idx}`);
              if (obj.type == 'section') {
                SectionControl.change(this.hotspots[idx].target);
              } else {
                settings.character.last = settings.character.current;
                settings.character.current = obj.target;
                SectionControl.change('characters');
              }
            }).bind(this)
          );

          if (typeof this.panorama[obj.pano] !== 'undefined') this.panorama[obj.pano].add(this.hotspots[idx].el);

          break;

        case 'character':
        case 'video':
          var texture = null;
          if (typeof obj.video !== 'undefined' && this.webMSupported) {
            // if (obj.art.indexOf('.png') < 0) {
            this.hotspots[idx].video = document.createElement('video');

            this.hotspots[idx].video.volume = 0.0;
            this.hotspots[idx].video.playsInline = true;
            this.hotspots[idx].video.muted = true;
            this.hotspots[idx].video.autoplay = true;
            this.hotspots[idx].video.src = obj.video;
            this.hotspots[idx].video.load();
            this.hotspots[idx].video.play();
            texture = new THREE.VideoTexture(this.hotspots[idx].video);
          } else {
            texture = new THREE.TextureLoader().load(obj.art);
          }
          texture.needsUpdate;
          texture.minFilter = THREE.LinearFilter;
          texture.magFilter = THREE.LinearFilter;
          texture.format = THREE.RGBAFormat;
          texture.crossOrigin = 'anonymous';
          this.hotspots[idx].target = obj.target;
          this.hotspots[idx].hover = [];
          this.hotspots[idx].el = new THREE.Mesh(
            new THREE.PlaneGeometry(obj.width, obj.height),
            new THREE.MeshBasicMaterial({ map: texture, transparent: true })
            // new THREE.MeshBasicMaterial({ map: texture, transparent: true, blending: THREE.AdditiveBlending  })
          );

          var box = new THREE.Box3().setFromObject(this.hotspots[idx].el);
          box.getCenter(this.hotspots[idx].el.position);
          this.hotspots[idx].el.position.multiplyScalar(-1);
          this.hotspots[idx].pivot = new THREE.Group();
          this.hotspots[idx].pivot.add(this.hotspots[idx].el);
          this.hotspots[idx].pivot.position.set(...obj.position);

          if (typeof obj.rotation !== 'undefined') {
            this.hotspots[idx].pivot.rotation.x = obj.rotation[0];
            this.hotspots[idx].pivot.rotation.y = obj.rotation[1];
            this.hotspots[idx].pivot.rotation.z = obj.rotation[2];
          }
          this.hotspots[idx].pivot.scale.x = obj.scale;
          this.hotspots[idx].pivot.scale.y = obj.scale;

          obj.hover.forEach((v, i) => {
            this.hotspots[idx].hover[i] = {};
            var hoverTexture = new THREE.TextureLoader().load(v.art);
            hoverTexture.needsUpdate;
            hoverTexture.minFilter = THREE.LinearFilter;
            hoverTexture.magFilter = THREE.LinearFilter;
            hoverTexture.format = THREE.RGBAFormat;
            hoverTexture.crossOrigin = 'anonymous';

            this.hotspots[idx].hover[i].el = new THREE.Mesh(
              new THREE.PlaneGeometry(v.width, v.height),
              new THREE.MeshBasicMaterial({ map: hoverTexture, transparent: true })
            );
            var hoverBox = new THREE.Box3().setFromObject(this.hotspots[idx].hover[i].el);
            hoverBox.getCenter(this.hotspots[idx].hover[i].el.position);
            this.hotspots[idx].hover[i].el.position.multiplyScalar(-1);
            this.hotspots[idx].hover[i].pivot = new THREE.Group();
            this.hotspots[idx].hover[i].pivot.add(this.hotspots[idx].hover[i].el);
            this.hotspots[idx].hover[i].pivot.position.set(...v.position);
            if (typeof obj.rotation !== 'undefined') {
              this.hotspots[idx].hover[i].pivot.rotation.x = v.rotation[0];
              this.hotspots[idx].hover[i].pivot.rotation.y = v.rotation[1];
              this.hotspots[idx].hover[i].pivot.rotation.z = v.rotation[2];
            }
            this.hotspots[idx].hover[i].pivot.scale.x = v.scale;
            this.hotspots[idx].hover[i].pivot.scale.y = v.scale;
            this.hotspots[idx].hover[i].el.addEventListener(
              'hoverenter',
              (evt => {
                if (!this.enableUpdate) return;
                document.querySelector('#sitePanos').style.cursor = 'pointer';
                if (this.hotspots[idx].video && !this.hotspots[idx].video.playing) this.hotspots[idx].video.play();
                this.hotspots[idx].hover.forEach((el, i) => {
                  this.hotspots[idx].hover[i].pivot.visible = true;
                });
              }).bind(this)
            );
            this.hotspots[idx].hover[i].el.addEventListener(
              'hoverleave',
              (evt => {
                if (!this.enableUpdate) return;
                document.querySelector('#sitePanos').style.cursor = 'default';
                this.hotspots[idx].hover.forEach((el, i) => {
                  this.hotspots[idx].hover[i].pivot.visible = false;
                });
              }).bind(this)
            );

            this.hotspots[idx].hover[i].pivot.visible = false;
            this.panorama[obj.pano].add(this.hotspots[idx].hover[i].pivot);
          });

          this.hotspots[idx].el.addEventListener(
            'click',
            (() => {
              if (!this.enableUpdate) return;
              settings.character.last = settings.character.current;
              settings.character.current = this.hotspots[idx].target;
              SectionControl.change('character');
            }).bind(this)
          );

          this.hotspots[idx].el.addEventListener(
            'hoverenter',
            (evt => {
              if (!this.enableUpdate) return;

              document.querySelector('#sitePanos').style.cursor = 'pointer';
              if (this.hotspots[idx].video && !this.hotspots[idx].video.playing) this.hotspots[idx].video.play();
              this.hotspots[idx].hover.forEach((el, i) => {
                this.hotspots[idx].hover[i].pivot.visible = true;
              });
            }).bind(this)
          );
          this.hotspots[idx].el.addEventListener(
            'hoverleave',
            (evt => {
              if (!this.enableUpdate) return;

              document.querySelector('#sitePanos').style.cursor = 'default';
              this.hotspots[idx].hover.forEach((el, i) => {
                this.hotspots[idx].hover[i].pivot.visible = false;
              });
            }).bind(this)
          );

          this.panorama[obj.pano].add(this.hotspots[idx].pivot);
          // this.panorama[obj.pano].add(this.hotspots[idx].el);

          break;

        default:
          break;
      }
    });
  }
  removePanoObjects() {}

  // EVENT HANDLING

  onPanoChange(ref) {
    if (!this.enableUpdate) return;

    if (typeof this.dust !== undefined) {
      this.dust.pivot.rotation.z += 0.0005;
      this.dust.pivot2.rotation.z += 0.0005;
    }

    this.hotspots.forEach(obj => {
      const el = obj.pivot || null;
      if (el !== null && el.type !== 'infospot') el.lookAt(this.camera.position);
    });

    this.renderer.render(this.scene, this.camera);
  }

  onMouseMove(evt) {
    var mouse = new THREE.Vector2();
    mouse.x = (evt.clientX / window.innerWidth) * 2 - 1;
    mouse.y = -(evt.clientY / window.innerHeight) * 2 + 1;

    var raycaster = new THREE.Raycaster();
    raycaster.setFromCamera(mouse, this.viewer.getCamera());
    var intersects = raycaster.intersectObjects(this.hotspots);

    if (intersects.length > 0) {
      document.body.style.cursor = 'pointer';
    } else {
      document.body.style.cursor = 'default';
    }
  }
  onResize(sizes) {
    const w = window.innerWidth;
    const h = window.innerHeight;
    this.viewer.container.style.width = w + 'px';
    this.viewer.container.style.height = h + 'px';
    this.viewer.onWindowResize(w, h);
  }

  // UTILS

  //use to move pano items around and get value
  //HACKY, ADD ABILITY TO HOT SWAP BETWEEN OBJECTS
  editInfospots(enabled, selSpot) {
    if (!enabled) return;

    document.body.addEventListener(
      'keydown',
      (evt => {
        evt.preventDefault();
        evt.stopPropagation();

        let currentPano = selSpot;
        let curObj = this.hotspots[currentPano].pivot
          ? this.hotspots[currentPano].pivot
          : this.hotspots[currentPano].el;

        //override
        // curObj = this.hotspots[currentPano].hover[0].pivot;
        curObj = this.dust.pivot;

        var event = window.event ? window.event : evt;
        console.log(event.keyCode);
        // console.log('before',curObj.position);
        switch (event.keyCode) {
          case 34: //pageup =
            // curObj.scale.x -= 1;
            curObj.scale.x -= 0.1;
            // curObj.scale.y -= 1;
            curObj.scale.y -= 0.1;
            break;

          case 33: //pagedown =
            // curObj.scale.x += 1;
            curObj.scale.x += 0.1;
            // curObj.scale.y += 1;
            curObj.scale.y += 0.1;
            break;

          case 36: //home =
            curObj.rotation.y -= 0.1;
            break;

          case 35: //end =
            curObj.rotation.y += 0.1;
            break;

          case 37: //left =
            curObj.position.x -= 10;
            break;

          case 38: //up =
            curObj.position.y -= 10;
            break;

          case 39: //right =
            curObj.position.x += 10;
            break;

          case 40: //down =
            curObj.position.y += 10;
            break;

          case 107: // + =
            curObj.position.z += 10;
            break;

          case 109: // - =
            curObj.position.z -= 10;
            break;

          default:
            break;
        }
        console.log('after', curObj.position, curObj.rotation, curObj.scale.x);
      }).bind(this)
    );
  }
  fromElToSVG(el) {
    var chld = el.childNodes;
    return new Promise((resolve, reject) => {
      this.resolve = resolve();
      try {
        chld.forEach(
          (v => {
            this.applyStyle(v);
          }).bind(this)
        );
        this.applyStyle(el);
      } catch (e) {
        reject();
      }
      var tmplt = document.querySelector('#convTemplate');
      tmplt.content.querySelector('#conversionContent').innerHTML = el;

      var clone = document.importNode(t.content, true);
      var img = new Image();
      // img.onload = callback;
      img.src = clone;
      resolve(img);
    });
  }
  applyStyle(el) {
    s = getComputedStyle(el);
    for (let key in s) {
      let prop = key.replace(/\-([a-z])/g, v => v[1].toUpperCase());
      el.style[prop] = s[key];
    }
  }

  //if using webm videos for alpha capabilities, this will check. make sure to include fallback image
  supportsWebMAlpha(callback) {
    var vid = document.createElement('video');
    vid.autoplay = false;
    vid.loop = false;
    vid.style.display = 'none';
    vid.addEventListener(
      'loadeddata',
      function() {
        document.body.removeChild(vid);
        // Create a canvas element, this is what user sees.
        var canvas = document.createElement('canvas');

        //If we don't support the canvas, we definitely don't support webm alpha video.
        if (!(canvas.getContext && canvas.getContext('2d'))) {
          callback(false);
          return;
        }
        // Get the drawing context for canvas.
        var ctx = canvas.getContext('2d');

        // Draw the current frame of video onto canvas.
        ctx.drawImage(vid, 0, 0);
        if (ctx.getImageData(0, 0, 1, 1).data[3] === 0) {
          callback(true);
        } else {
          callback(false);
        }
      },
      false
    );
    vid.addEventListener('error', function() {
      document.body.removeChild(vid);
      callback(false);
    });

    vid.addEventListener('stalled', function() {
      document.body.removeChild(vid);
      callback(false);
    });

    //Just in case
    vid.addEventListener('abort', function() {
      document.body.removeChild(vid);
      callback(false);
    });
    var source = document.createElement('source');
    source.src =
      'data:video/webm;base64,GkXfowEAAAAAAAAfQoaBAUL3gQFC8oEEQvOBCEKChHdlYm1Ch4ECQoWBAhhTgGcBAAAAAAACBRFNm3RALE27i1OrhBVJqWZTrIHlTbuMU6uEFlSua1OsggEjTbuMU6uEHFO7a1OsggHo7AEAAAAAAACqAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAVSalmAQAAAAAAADIq17GDD0JATYCNTGF2ZjU3LjU3LjEwMFdBjUxhdmY1Ny41Ny4xMDBEiYhARAAAAAAAABZUrmsBAAAAAAAARq4BAAAAAAAAPdeBAXPFgQGcgQAitZyDdW5khoVWX1ZQOYOBASPjg4QCYloA4AEAAAAAAAARsIFAuoFAmoECU8CBAVSygQQfQ7Z1AQAAAAAAAGfngQCgAQAAAAAAAFuhooEAAACCSYNCAAPwA/YAOCQcGFQAADBgAABnP///NXgndmB1oQEAAAAAAAAtpgEAAAAAAAAk7oEBpZ+CSYNCAAPwA/YAOCQcGFQAADBgAABnP///Vttk7swAHFO7awEAAAAAAAARu4+zgQC3iveBAfGCAXXwgQM=';
    source.addEventListener('error', function() {
      document.body.removeChild(vid);
      callback(false);
    });
    vid.appendChild(source);
    //This is required for IE
    document.body.appendChild(vid);
  }
}
export default PanoHandler;
