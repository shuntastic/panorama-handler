class panoData {
  constructor() {
    return {
      panos: [
        {
          visible: true,
          name: 'home2',
          source: 'fpo/images/test_pano_1.jpg'
        },
        {
          visible: true,
          name: 'home3',
          source: 'fpo/images/test_pano_2.jpeg'
        },
        {
          visible: true,
          name: 'home4',
          source: 'fpo/images/test_pano_3.jpg'
        },
        {
          visible: true,
          name: 'home1',
          source: 'fpo/images/IMG_6548.HEIC'
        },
      ],
      hotpsots: [
        // MAIN ROOM LINKS
        {
          // label: 'ABOUT THE MOVIE',
          type: 'section',
          visible: true,
          pano: 'home',
          target: 'home2',
          art: 'assets/images/home/panobtn_about.png',
          // width: 443,
          // height: 168,
          position: [0, 20, 80],
          scale: 320
        },
        // {
        //   // label: 'BETSY',
        //   type: 'character',
        //   width: 728,
        //   height: 1368,
        //   visible: true,
        //   pano: 'home',
        //   target: 'betsy',
        //   video: 'assets/videos/betsy.webm',
        //   art: 'assets/images/home/betsy.png',
        //   position: [950, -80, -700],
        //   rotation: [0, 30, 0],
        //   scale: 1.0,
        //   hover: [
        //     {
        //       art: 'assets/images/home/hotspot_betsy.png',
        //       width: 241,
        //       height: 53,
        //       position: [760, 180, -540],
        //       rotation: [0, -1.4, 0],
        //       scale: 1.0
        //     }
        //   ]
        // },
      ]
    };
  }
}

export default new panoData();
